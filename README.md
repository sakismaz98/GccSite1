#GCC SITE
**Install requirements.txt**

to deploy a server on terminal go to path gccBlog/GccBlogSite

and run `python manage.py runserver`

to create admin user : `python manage.py createsuperuser`

when you update/delete/create a model you need to run

`python manage.py makemigrations`

`python manage.py migrate`