from django.contrib import admin
from .models import Post
from ckeditor.fields import RichTextField, CKEditorWidget
from ckeditor import widgets
from tinymce.widgets import TinyMCE
from django.db import models
# Register your models here.


class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'author', 'slug', 'status', 'created_on')
    list_filter = ("status",)
    search_fields = ['title', 'content']
    prepopulated_fields = {'slug': ('title',)}

    formfield_overrides = {
        models.TextField: {'widget': (TinyMCE())}
    }

admin.site.register(Post, PostAdmin)